import {Component, Inject, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {ExtraValidators} from "../../core/validators/extra.validator";
import {FORM_ERRORS} from "../../core/providers/form-errors.provider";
import {v4 as uuidv4} from 'uuid';
import * as moment from 'moment';
import {MessageDeliveryReport, MessageNotRoutedReasons, MessageUnicode} from "../../core/enums/message.enum";


@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.scss']
})
export class MessageFormComponent implements OnInit {
  notRoutedReasonsOptions = MessageNotRoutedReasons;
  deliverReportOptions = MessageDeliveryReport;
  messageForm: FormGroup;
  title = 'Update message'
  @Input() message = null;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<MessageFormComponent>,
              @Inject(FORM_ERRORS) private formErrors) {
  }

  ngOnInit() {
    this.messageForm = new FormGroup({
      poolNumber: new FormControl(this.data?.message?.poolNumber
        || '', [Validators.required, Validators.min(0), Validators.max(1000)]),
      sender: new FormControl(this.data?.message?.sender
        || '', [ExtraValidators.stringNumber12]),
      notRoutedReason: new FormControl(this.data?.message?.notRoutedReason
        || '', Validators.required),
      sentDate: new FormControl(moment(this.data?.message?.sentDateTime).format(moment.HTML5_FMT.DATE)
        || moment().format(moment.HTML5_FMT.DATE), Validators.required),
      sentTime: new FormControl(moment(this.data?.message?.sentDateTime).format('HH:mm:ss')
        || moment().format('HH:mm:ss'), Validators.required),
      requestedDeliveryReportMaskText: new FormControl(this.data?.message?.requestedDeliveryReportMaskText
        || '', Validators.required),
      deliveryReportReceivedDate: new FormControl(moment(this.data?.message?.deliveryReportReceivedDateTime).format(moment.HTML5_FMT.DATE)
        || moment().format(moment.HTML5_FMT.DATE), Validators.required),
      deliveryReportReceivedTime: new FormControl(moment(this.data?.message?.deliveryReportReceivedDateTime).format('HH:mm:ss')
        || moment().format('HH:mm:ss'), Validators.required),
      isUnicode: new FormControl(this.data?.message?.isUnicode === 'TRUE'
        || false, Validators.required),
    });
  }

  get formControls() {
    return this.messageForm.controls;
  }

  saveMessage(): void {
    if (this.messageForm.valid) {
      this.dialogRef.close({
        event: this.data?.message?.messageUUID ? 'UPDATE' : 'CREATE',
        data: {
          messageUUID: this.data?.message?.messageUUID || uuidv4(),
          poolNumber: String(this.messageForm.get('poolNumber').value),
          sender: this.messageForm.get('sender').value,
          notRoutedReason: this.messageForm.get('notRoutedReason').value,
          sentDateTime: this.getDateTime(this.messageForm.get('sentDate'), this.messageForm.get('sentTime')),
          requestedDeliveryReportMaskText: this.messageForm.get('requestedDeliveryReportMaskText').value,
          deliveryReportReceivedDateTime: this.getDateTime(this.messageForm.get('deliveryReportReceivedDate'),
            this.messageForm.get('deliveryReportReceivedTime')),
          isUnicode: String(this.messageForm.get('isUnicode').value).toUpperCase(),
        }
      });
    }
  }

  close(): void {
    this.dialogRef.close();
  }

  private getDateTime(controlDate: AbstractControl, controlTime: AbstractControl): string {
    const time = controlTime.value?.split(':');
    const date = moment(controlDate.value)
    return date.set({
      hour: time[0],
      minute: time[1],
      second: time[2]
    }).utc().format();
  }
}
