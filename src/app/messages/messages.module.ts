import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MessageFormComponent} from "./message-form/message-form.component";
import {MessagesRoutingModule} from "./messages-routing";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MessageListComponent} from './message-list/message-list.component';
import {MessageCsvFormComponent} from './message-csv-form/message-csv-form.component';
import {MatButtonModule} from "@angular/material/button";
import {MatFormFieldModule} from "@angular/material/form-field";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatInputModule} from "@angular/material/input";
import {MatCheckboxModule} from "@angular/material/checkbox";
import {MatDividerModule} from "@angular/material/divider";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSortModule} from "@angular/material/sort";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatMenuModule} from "@angular/material/menu";
import { MessageListFiltersComponent } from './message-list/message-list-filters/message-list-filters.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {ConfirmDialogModule} from "../shared/confirm-dialog/confirm-dialog.module";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from "@angular/material-moment-adapter";
import {MatSelectModule} from "@angular/material/select";
import {ParseBoolPipe} from "../core/pipes/parse-bool.pipe";
import {EnumToArrayPipe} from "../core/pipes/enum-to-array.pipe";

@NgModule({
  declarations: [
    MessageFormComponent,
    MessageListComponent,
    MessageCsvFormComponent,
    MessageListFiltersComponent,
    ParseBoolPipe,
    EnumToArrayPipe
  ],
  imports: [
    CommonModule,
    MessagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatFormFieldModule,
    FlexLayoutModule,
    MatInputModule,
    MatDividerModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatSortModule,
    MatPaginatorModule,
    MatMenuModule,
    MatSnackBarModule,
    ConfirmDialogModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatSelectModule
  ],
  providers: [

  ]
})
export class MessagesModule {
}
