import {Component, Input, OnInit} from '@angular/core';
import {Message} from "../../../core/models/message";
import {MatTableDataSource} from "@angular/material/table";
import {MessageDeliveryReport, MessageNotRoutedReasons, MessageUnicode} from "../../../core/enums/message.enum";
import * as moment from 'moment';

interface MatTableFilter {
  key: string;
  value: any;
}

@Component({
  selector: 'app-message-list-filters[dataSource]',
  templateUrl: './message-list-filters.component.html',
  styleUrls: ['./message-list-filters.component.scss']
})
export class MessageListFiltersComponent implements OnInit {
  filters: MatTableFilter[] = [];
  notRoutedReasonsOptions = MessageNotRoutedReasons;
  deliverReportOptions = MessageDeliveryReport;
  messageUnicodeOptions = MessageUnicode;

  @Input() dataSource: MatTableDataSource<Message>

  constructor() {
  }

  ngOnInit(): void {
    this.dataSource.filterPredicate = (data, filter) => {
      let jsonFilter: MatTableFilter[] = JSON.parse(filter);
      let result = null;
      for (let j = 0; j < jsonFilter.length; j++) {
        let i = 0;
        do {
          if (result !== false) {
             if (data[this.filters[i].key] && this.filters[i].key) {
              result = data[this.filters[i].key].toLowerCase().includes(this.filters[i].value);
            } else {
              result = false;
            }
          }
          i++;
        } while (i < this.filters.length);
      }
      return result;
    }
  }


  applyFilter(filterValue: string, key: string) {
    const value = filterValue ? filterValue.trim().toLowerCase() : ''
    const i = this.filters.findIndex(x => x.key == key)
    if (i === -1) {
      this.filters.push({value, key});
    } else {
      this.filters[i].value = value
    }
    this.dataSource.filter = JSON.stringify(this.filters);
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
