import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MessagesService} from "../../core/services/messages.service";
import {Observable, Subscription} from "rxjs";
import {Message} from "../../core/models/message";
import {MatTableDataSource} from "@angular/material/table";
import {MatDialog} from "@angular/material/dialog";
import {BreakpointObserver, Breakpoints, BreakpointState} from "@angular/cdk/layout";
import {MessageFormComponent} from "../message-form/message-form.component";
import {MessageCsvFormComponent} from "../message-csv-form/message-csv-form.component";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort, MatSortable} from "@angular/material/sort";
import {ConfirmDialogComponent} from "../../shared/confirm-dialog/confirm-dialog.component";
import {CsvJsonService} from "../../core/services/csv-json.service";
import {MatSnackBar, MatSnackBarRef} from "@angular/material/snack-bar";

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

@Component({
  selector: 'app-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss']
})
export class MessageListComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription = new Subscription();
  dataSource = new MatTableDataSource<Message>([]);
  displayedColumns = ['Message UUID', 'Sender', 'Pool number', 'Not Routed Reasons', 'Sent Date Time',
    'Requested Delivery Report Mask Text', 'Delivery Report Received', 'Is Unicode', 'Actions'];
  paginatorOptions = [15, 30, 50, 100];
  snackbarInstance: MatSnackBarRef<any>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private messagesService: MessagesService,
              private dialog: MatDialog,
              private breakpointObserver: BreakpointObserver,
              private csvJsonService: CsvJsonService,
              private snackBar: MatSnackBar) {
  }

  get hasData() {
    return this.dataSource.data.length > 0
  }

  ngOnInit() {
    this.subscriptions.add(
      this.messagesService.messages.subscribe((result) => {
        this.dataSource.data = result;
      })
    );
    this.messagesService.load();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = (data, attribute) => data[attribute];
    this.sort?.sort(({id: 'sender', start: 'asc'}) as MatSortable);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

  confirmDelete(message) {
    this.subscriptions.add(
      this.dialog
        .open(ConfirmDialogComponent, {
          data: {
            title: 'Confirm message delete',
            message: `Are you sure that you want to delete message with uid ${message.messageUUID}`
          }
        })
        .afterClosed()
        .subscribe(result => {
          if (result) {
            this.messagesService.delete(message.messageUUID);
            this.snackbarInstance = this.snackBar.open('You successfully deleted the message', '', {
              panelClass: ['success']
            });
          }
        })
    );
  }

  loadCsv() {
    this.dialog.open(MessageCsvFormComponent)
  }

  showCreateUpdate(message?) {
    this.subscriptions.add(
      this.dialog
        .open(MessageFormComponent, {
          maxWidth: '800px',
          data: {message}
        })
        .afterClosed()
        .subscribe(result => {
          if (result?.event === 'UPDATE') {
            this.messagesService.update(message.messageUUID, result.data);
            this.snackbarInstance = this.snackBar.open('You successfully updated the message', '', {
              panelClass: ['success']
            });
          } else if (result?.event === 'CREATE') {
            this.messagesService.create(result.data);
            this.snackbarInstance = this.snackBar.open('You successfully created the message', '', {
              panelClass: ['success']
            });
          }
        })
    );
  }

  download(data) {
    this.csvJsonService.downloadJson(data)
  }
}
