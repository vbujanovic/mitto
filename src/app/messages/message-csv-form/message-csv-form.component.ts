import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {MessagesService} from "../../core/services/messages.service";
import {MatDialogRef} from "@angular/material/dialog";
import {MatSnackBar, MatSnackBarRef} from "@angular/material/snack-bar";

@Component({
  selector: 'app-message-csv-form',
  templateUrl: './message-csv-form.component.html',
  styleUrls: ['./message-csv-form.component.scss']
})
export class MessageCsvFormComponent  {
  title = "Load CSV file"
  snackbarInstance: MatSnackBarRef<any>;
  csvForm = this.formBuilder.group({
    file: ['', [Validators.required]]
  });

  constructor(private formBuilder: FormBuilder, private cd: ChangeDetectorRef,
              public dialogRef: MatDialogRef<MessageCsvFormComponent>,
              private messagesService: MessagesService,
              private snackBar: MatSnackBar) { }

  submitCsv() {
    if (this.csvForm.valid) {
      this.messagesService.loadCsv(this.csvForm.get('file').value);
      this.csvForm.reset();
      this.dialogRef.close(true);
      this.snackbarInstance = this.snackBar.open('You successfully loaded CSV file', '', {
        panelClass: ['success']
      });
    }
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;

      reader.readAsText(file);
      reader.onload = () => {
        this.csvForm.patchValue({
          file: reader.result
        });
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
      reader.onerror = () => {
        // handle error
      }
    }
  }

  close(): void {
    this.dialogRef.close();
  }
}
