export interface Message {
  messageUUID: string;
  sender: string;
  poolNumber: number;
  notRoutedReason: string | number;
  sentDateTime: string;
  requestedDeliveryReportMaskText: string;
  deliveryReportReceivedDateTime: string;
  isUnicode: boolean;
}
