import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'parseBool'
})
export class ParseBoolPipe implements PipeTransform {

  transform(value: string | boolean): boolean {
    switch (value.toString().toLowerCase()) {
      case "true":
        return true;
      case "false":
        return false;
      default:
        return null;
    }
  }

}
