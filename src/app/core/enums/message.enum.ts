export enum MessageNotRoutedReasons {
  NumberDoesntExist,
  NoPrefixAvailable,
  NoApplicableRouteFound
}

export enum MessageDeliveryReport {
  InProgress,
  Submitted,
  Failed
}

export enum MessageUnicode {
  TRUE,
  FALSE
}
