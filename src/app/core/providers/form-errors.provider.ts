import {InjectionToken} from '@angular/core';

export const defaultErrors = {
  required: () => `This field is required`,
  minlength: ({requiredLength, actualLength}) => `Expect ${requiredLength} but got ${actualLength}`,
  stringNumber12: () => `Text or a number (12 chars min)`,
  min: (min, max) => `Expect number between ${min} and ${max}`,
  max: (min, max) => `Expect number between ${min} and ${max}`

};

export const FORM_ERRORS = new InjectionToken('FORM_ERRORS', {
  providedIn: 'root',
  factory: () => defaultErrors
});
