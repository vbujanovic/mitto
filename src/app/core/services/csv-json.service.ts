import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class CsvJsonService {
  public csvToJson(parsedCsv) {
    const lines = parsedCsv.split(/\r?\n/);
    const fieldDelimiter = ',';
    const headers = lines[0].split(fieldDelimiter);

    const jsonResult = [];
    for (let i = 1; i < lines.length; i++) {
      if (lines[i].trim()?.length > 0) {
        const currentLine = lines[i].split(fieldDelimiter)
        jsonResult.push(CsvJsonService.buildJsonResult(headers, currentLine));
      }
    }
    return jsonResult;
  }

  private static buildJsonResult(headers, currentLine) {
    const jsonObject = {};
    for (let j = 0; j < headers.length; j++) {
      const propertyName = headers[j].trim()
      jsonObject[propertyName] = currentLine[j];
    }
    return jsonObject;
  }

  public downloadJson(data) {
    const sJson = JSON.stringify(data);
    const element = document.createElement('a');
    element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(sJson));
    element.setAttribute('download', "messages-json.json");
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
    return data;
  }
}
