import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {Message} from "../models/message";
import {CsvJsonService} from "./csv-json.service";
import {BrowserStorageService} from "./browser-storage.service";

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  private key = 'messages';
  private messagesSubject = new BehaviorSubject<Message[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(true);
  messages = this.messagesSubject.asObservable();
  loading = this.loadingSubject.asObservable();

  constructor(private csvJsonService: CsvJsonService,
              private browserStorage: BrowserStorageService) {
  }

  load() {
    const data = this.browserStorage.getLocal(this.key)
    if (data) {
      this.messagesSubject.next(data);
    }
  }

  loadCsv(csvString: string | ArrayBuffer) {
    const data = this.csvJsonService.csvToJson(csvString);
    this.browserStorage.setLocal(this.key, data);
    this.messagesSubject.next(data);
  }

  create(data) {
    const updatedData = this.browserStorage.getLocal(this.key);
    updatedData.push(data);
    this.browserStorage.setLocal(this.key, updatedData);
    this.messagesSubject.next(updatedData);
  }

  delete(uuid) {
    const updatedData = this.browserStorage.getLocal(this.key).filter(o => o?.messageUUID !== uuid);
    this.browserStorage.setLocal(this.key, updatedData);
    this.messagesSubject.next(updatedData);
  }

  update(uuid, data) {
    const updatedData = this.browserStorage.getLocal(this.key).map((o) => o?.messageUUID === uuid ? data : o)
    this.browserStorage.setLocal(this.key, updatedData);
    this.messagesSubject.next(updatedData);
  }
}
