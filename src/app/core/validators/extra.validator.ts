import {AbstractControl} from "@angular/forms";

export class ExtraValidators {
  static stringNumber12(control: AbstractControl): { [key: string]: boolean } | null {
    if (control.value) {
      const value = +control.value;
      if (Number.isNaN(value)) {
        return null;
      } else {
        if (control.value.split('').length > 11) {
          return null
        }
      }
    }
    return { stringNumber12: true };
  }
}

